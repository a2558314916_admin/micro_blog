package com.mucd.blog.util;

import com.mucd.blog.statuscode.MyException;
import com.mucd.blog.statuscode.StatusCode;

/**
 * @author mucd
 */
public class AssertUtil {

    /**
     * 判断是否重复
     *
     * @param b
     */
    public static void isRepeat(Boolean b) {
        if (!b) {
            throw new MyException(StatusCode.EXCEPTION_ERROR);
        }
    }

}

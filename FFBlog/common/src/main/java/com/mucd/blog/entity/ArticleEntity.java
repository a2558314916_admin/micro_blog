package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 文章
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("article")
public class ArticleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 文章id
	 */
	@TableId
	private Long articleId;
	/**
	 * 标题
	 */
	private String articleTitle;
	/**
	 * 文章内容
	 */
	private String articleContent;
	/**
	 * 分类
	 */
	private String articleClassify;
	/**
	 * 标签
	 */
	private String articleTags;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private LocalDateTime createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private LocalDateTime updateTime;
	/**
	 * 发布人
	 */
	private String articleUsername;
	/**
	 * 文章排行
	 */
	private Integer articleRanking;
	/**
	 * 封面
	 */
	private String articleCover;
	/**
	 * 收藏数
	 */
	private Integer articleCollect;
	/**
	 * 点赞数
	 */
	private Integer articleLike;
	/**
	 * 不喜欢数量
	 */
	private Integer articleUnlike;

}

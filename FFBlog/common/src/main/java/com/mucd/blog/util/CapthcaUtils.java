package com.mucd.blog.util;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.mucd.blog.result.R;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
public class CapthcaUtils {

    public static R createCapthca(){
        //定义图形验证码的长、宽、验证码字符数、干扰元素个数
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 20);
        //CircleCaptcha captcha = new CircleCaptcha(200, 100, 4, 20);
        //图形验证码写出，可以写出到文件，也可以写出到流
        //验证图形验证码的有效性，返回boolean值
        return R.ok(captcha.getCode());
    }

}

package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Objects;

/**
 * 栏目(Column)实体类
 *
 * @author makejava
 * @since 2022-06-05 21:03:50
 */
@TableName("columns")
public class ColumnEntity implements Serializable {
    private static final long serialVersionUID = 246421748007947272L;
    /**
     * 栏目id
     */
    @TableId(value = "column_id", type = IdType.AUTO)
    private Integer columnId;
    /**
     * 栏目名
     */
    @TableField("column_name")
    private String columnName;
    /**
     * 栏目状态
     * 1 正常
     * 0 删除
     */
    @TableField("column_status")
    private Integer columnStatus;
    /**
     * 栏目路由
     */
    @TableField("column_link")
    private String columnLink;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColumnEntity that = (ColumnEntity) o;
        return Objects.equals(columnId, that.columnId) && Objects.equals(columnName, that.columnName) && Objects.equals(columnStatus, that.columnStatus) && Objects.equals(columnLink, that.columnLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnId, columnName, columnStatus, columnLink);
    }

    public String getColumnLink() {
        return columnLink;
    }

    public void setColumnLink(String columnLink) {
        this.columnLink = columnLink;
    }

    public Integer getColumnId() {
        return columnId;
    }

    public void setColumnId(Integer columnId) {
        this.columnId = columnId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getColumnStatus() {
        return columnStatus;
    }

    public void setColumnStatus(Integer columnStatus) {
        this.columnStatus = columnStatus;
    }

}


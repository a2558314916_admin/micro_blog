package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 标签
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("blog_tags")
public class TagsEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 标签id
     */
    @TableId(type = IdType.AUTO)
    private Long tagsId;
    /**
     * 标签名
     */
    private String tagsName;
    /**
     * 标签下文章数
     */
    private Integer tagsArticleNum;

    /**
     * 标签链接
     */
    private String tagsLink;
    /**
     * 标签图表
     */
    private String tagsIcon;
}

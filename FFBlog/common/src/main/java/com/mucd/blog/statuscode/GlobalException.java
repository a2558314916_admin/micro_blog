package com.mucd.blog.statuscode;

import com.mucd.blog.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
@RestControllerAdvice
@Slf4j
public class GlobalException {

    @ExceptionHandler(RuntimeException.class)
    public R runtimeException(Exception e) {
        log.error(e.getMessage());

        return R.error("未知异常,请联系管理员");
    }
}

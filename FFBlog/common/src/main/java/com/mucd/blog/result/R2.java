package com.mucd.blog.result;

import lombok.Data;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;


/**
 * @Author mucd
 */
@Data
@Scope("prototype")
public class R2<K, V> extends HashMap<K, V> {

    private static String msg;
    private static Integer code;
    private static HashMap<String, Object> data;


    public R2() {
    }

    public static R2<Object, Object> ok() {
        code = 200;
        return new R2<>();
    }

    public static R2<Object, Object> err() {
        code = 500;
        return new R2<>();
    }

}

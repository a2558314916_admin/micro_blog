package com.mucd.blog.block;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.result.R;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
public class BlockHandles {

    public static R blockHandleLogin(MyUserEntity user, BlockException b) {
//        throw new MyException(StatusCode.SENTINEL_BANK);
        return R.error("当前人数过多,请稍后再试!");
    }
}

package com.mucd.blog.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.mucd.blog.entity.TagsEntity;

/**
 * @author mucd
 * @date 2022/5/31 2022/5/31
 */
public class ExceptionConfig {


    public static Object blockHandleTestB(BlockException be) {

        TagsEntity t = new TagsEntity();
        t.setTagsArticleNum(100);
        t.setTagsId(1L);
        t.setTagsName("name");

        System.out.println("message = " + be.toString());
        return t;
    }

}

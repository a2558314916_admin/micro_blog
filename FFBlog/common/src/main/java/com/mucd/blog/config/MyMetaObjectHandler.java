package com.mucd.blog.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义元数据对象处理器
 */
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 插入时自动填充字段
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        //设置账户创建时间为当前时间  获取当前时间:LocalDateTime.now()
        metaObject.setValue("createTime", LocalDateTime.now());
        //设置账户更新时间为当前时间
        metaObject.setValue("updateTime", LocalDateTime.now());

    }

    /**
     * 修改时自动填充字段
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        //设置账户修改时间为当前时间
        metaObject.setValue("updateTime", LocalDateTime.now());
    }
}

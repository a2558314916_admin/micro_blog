package com.mucd.blog.statuscode;

/**
 * @author mucd
 * @date 2022/5/31 2022/5/31
 */
public class MyException extends RuntimeException {
    private StatusCode satusCode;

    public MyException(StatusCode s) {
        super(s.getMessage());
        this.satusCode = s;
    }

}

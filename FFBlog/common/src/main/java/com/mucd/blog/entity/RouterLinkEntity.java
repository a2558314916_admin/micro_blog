package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 路由链接
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("router_link")
public class RouterLinkEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 路由id
	 */
	@TableId
	private Integer routerId;
	/**
	 * 路由地址
	 */
	private String routerName;
	/**
	 * 可以路由的角色
	 */
	private Integer routerRoleId;

}

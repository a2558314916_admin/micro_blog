package com.mucd.blog.statuscode;

/**
 * @author mucd
 * @date 2022/5/31 2022/5/31
 */
public enum StatusCode {

    USER_LOGIN_CODE("001_001", "验证码错误"),
    USER_LOGIN_ACCOUNT("001_002", "用户名或密码错误"),
    USER_VERIFY_PASS("001-003", "输入的旧密码错误"),
    USER_UPDATE("001-004", "输入的旧密码错误"),
    ARTICLE_ISOPEN("002-001", "修改文章是否公开失败"),
    ARTICLE_PUNISH("002-002", "发布文章失败"),
    ARTICLE_UPDATE("002-003", "修改文章失败"),
    ARTICLE_DELETE("002-004", "删除文章失败"),
    COLUMN_PUNISH("003-001", "新增栏目失败"),
    COLUMN_DELETE("003-002", "删除栏目失败"),
    COLUMN_UPDATE("003-003", "修改栏目失败"),
    TAGS_PUNISH("003-003", "新增文章标签失败"),
    TAGS_DELETE("003-003", "删除文章标签失败"),
    TAGS_UPDATE("003-003", "修改文章标签失败"),


    SENTINEL_BANK("003_001", "流量太大,请稍后再试"),
    SENTINEL_FUSING("003_002", "系统暂时熔断,请稍后重试"),
    SENTINEL_DEMOTION("003_003", "流量太大,改节点暂时降级,可以先看看别的内容"),

    TOKEN_OVERDUE("004_001", "token过期"),
    TOKEN_NULL("004_002", "token为空"),
    EXCEPTION_ERROR("005_001", "出了点小问题,请联系管理员"),
    ;


    //属于那个模块下的操作失败
    private String typeCode;
    //具体错误消息
    private String message;

    StatusCode(String typeCode, String message) {
        this.typeCode = typeCode;
        this.message = message;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
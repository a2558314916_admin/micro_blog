package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("user")
public class MyUserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId
    private Long usernameId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 加密过后的密码
     */
    private String userPassword;
    /**
     * 访问量
     */
    private Integer userVisits;
    /**
     * 粉丝数量
     */
    private Long userFans;
    /**
     * 简介信息
     */
    private String userInfo;
    /**
     * 性别
     */
    private String userGender;
    /**
     * 用户地址
     */
    private String userAddress;
    /**
     * 生日
     */
    private Date userBirth;
    /**
     * 学校名称
     */
    private String userSchool;
    /**
     * 专业
     */
    private String userMajor;
    /**
     * 学历
     */
    private String userEducation;
    /**
     * 入学时间
     */
    private Date userSchoolTime;
    /**
     * 喜欢的标签
     */
    private String userLikeTag;
    /**
     * 角色id
     */
    private Integer userRoleId;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 用户头像
     */
    private String userHead;

}

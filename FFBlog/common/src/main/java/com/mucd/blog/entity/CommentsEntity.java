package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 评论
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("comments")
public class CommentsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 评论id
	 */
	@TableId
	private Long commentsId;
	/**
	 * 评论内容
	 */
	private String commentsContent;
	/**
	 * 评论的用户
	 */
	private String commentsUsername;
	/**
	 * 评论时间
	 */
	private Date commentsTime;
	/**
	 * 评论点赞
	 */
	private Integer commentsLike;
	/**
	 * 评论踩
	 */
	private Integer commentsUnlike;

}

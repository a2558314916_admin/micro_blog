package com.mucd.blog.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class R implements Serializable {
    private static final long serialVersionUID =  -6362049771743442342L;

    private String msg;
    private Integer code;
    private Object data;

    public R(String msg, Object data) {
        this.msg = msg;
        this.code = 200;
        this.data = data;
    }

    public R(String msg) {
        this.msg = msg;
        this.code = 500;
    }
    public R(Object data) {
        this.data = data;
        this.code = 200;
    }


    public static R error(String msg) {
        return new R(msg);
    }

    public static R ok(Object data) {
        return new R(data);
    }
}

package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * token表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("token")
public class TokenEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * tokenId
     */
    @TableId(type = IdType.AUTO)
    private Integer tokenId;
    /**
     * token值
     */
    private String tokenValue;
    /**
     * token创建时间
     */
    private Date tokenCreateTime;
    /**
     * token过期时间
     */
    private Date tokenExpiration;

}

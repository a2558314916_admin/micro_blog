package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 文章浏览记录
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("article_record")
public class ArticleRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long recordId;
	/**
	 * 浏览过的文章id
	 */
	private Long recordArticleId;
	/**
	 * 浏览用户
	 */
	private String recordUsername;
	/**
	 * 浏览时间
	 */
	private Date recordDatetime;

}

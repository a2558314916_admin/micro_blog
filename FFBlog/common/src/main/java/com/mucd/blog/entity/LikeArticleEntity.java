package com.mucd.blog.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 我的收藏
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2022-05-30 11:34:43
 */
@Data
@TableName("like_article")
public class LikeArticleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 收藏id
	 */
	@TableId
	private Long likeId;
	/**
	 * 收藏的用户
	 */
	private String likeUsername;
	/**
	 * 文章id
	 */
	private Long likeArticleId;
	/**
	 * 收藏目录名
	 */
	private String likeDir;
	/**
	 * 收藏时间
	 */
	private Date likeDate;
	/**
	 * 0正常;1删除
	 */
	private Integer likeStatus;

}

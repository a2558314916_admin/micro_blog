package com.mucd.blog.sentinel;

import cn.hutool.core.util.StrUtil;

import java.util.Arrays;
import java.util.HashSet;

/**
 * 为数组去重
 */
public class ArrayRemoval {
    public static HashSet<String> removal(String a) {
        String replace = a.replace("[", "");
        String replace1 = replace.replace("]", "");
        String replace2 = replace1.replace("\"", "");
        //去除空格
        String replace3 = StrUtil.cleanBlank(replace2);

        String[] split = replace3.split(",");

        HashSet<String> objects = new HashSet<>(Arrays.asList(split));

        return objects;
    }
}

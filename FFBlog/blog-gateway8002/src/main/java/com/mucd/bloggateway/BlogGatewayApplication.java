package com.mucd.bloggateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, WebMvcAutoConfiguration.class, RedisAutoConfiguration.class})
@EnableDiscoveryClient
@Slf4j
public class BlogGatewayApplication {

    public static void main(String[] args) {
        log.info("8002端口启动成功");
        SpringApplication.run(BlogGatewayApplication.class, args);
    }

}

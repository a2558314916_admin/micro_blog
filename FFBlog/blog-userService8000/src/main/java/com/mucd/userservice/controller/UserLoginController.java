package com.mucd.userservice.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.mucd.blog.annontion.NotNull;
import com.mucd.blog.block.BlockHandles;
import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.result.R;
import com.mucd.userservice.service.UserLoginServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserLoginController {

    @Resource
    private UserLoginServiceImpl userService;

    @RequestMapping("/login")
    @SentinelResource(value = "login", blockHandlerClass = BlockHandles.class, blockHandler = "blockHandleLogin")
    public R login(@NotNull @RequestBody MyUserEntity user) {
        return userService.login(user);
    }


    @RequestMapping("/captcha")
    public R captcha() {
        return userService.createCaptcha();
    }

}

package com.mucd.userservice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mucd.blog.entity.TokenEntity;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
public interface TokenDao extends BaseMapper<TokenEntity> {
}

package com.mucd.userservice.service;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.entity.TokenEntity;
import com.mucd.blog.result.R;
import com.mucd.blog.util.CapthcaUtils;
import com.mucd.blog.util.PasswordUtil;
import com.mucd.blog.util.TokenUtil;
import com.mucd.userservice.dao.TokenDao;
import com.mucd.userservice.dao.UserDao;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mucd
 * @date 2022/6/1 2022/6/1
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class UserLoginServiceImpl {

    @Resource
    private UserDao userDao;

    @Resource
    private TokenDao tokenDao;


    public R createCaptcha() {
        R captcha = CapthcaUtils.createCapthca();
        return captcha;
    }

    /**
     * 用户登录
     *
     * @param user
     * @return
     */
    public R login(MyUserEntity user) {
        Assert.notNull(user, "参数不能为空");
        //加密密码,拿着加密的密码去对比数据库
        String encryptPassword = PasswordUtil.encrypt(user.getUserPassword());


        QueryWrapper<MyUserEntity> qw = new QueryWrapper<>();
        qw.eq("user_name", user.getUserName());
        qw.eq("user_password", encryptPassword);

        MyUserEntity myUserEntity = userDao.selectOne(qw);

        //true 登录失败
        if (ObjectUtil.isNull(myUserEntity)) {
            return R.error("账号或密码错误");
        }

        //验证token是否存在 true 存在 false 不存在
        Map<String, Object> token1 = isToken(user);
        if ((Boolean) token1.get("status")) {
            //登录成功
            return R.ok(token1.get("token"));
        }

        //登录成功创建token
        TokenUtil tokenUtil = new TokenUtil();
        String token = tokenUtil.createToken(myUserEntity);
        //将token信息存到数据库
        saveToken(token, myUserEntity);

        //登录成功
        return R.ok(token);
    }

    /**
     * 登录成功保存token
     *
     * @param token
     */
    @SneakyThrows
    public void saveToken(String token, MyUserEntity user) {
        TokenUtil tokenUtil = new TokenUtil();
        Map<String, Object> map = tokenUtil.parseToken(token, user);

        //获取用户token信息
        String uid = String.valueOf(map.get("uid"));
        Long expireTime = Long.valueOf(String.valueOf(map.get("expire_time")));
        Long createTime = Long.valueOf(map.get("create_time").toString());

        //添加token信息到数据库
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setTokenValue(token);
        tokenEntity.setTokenCreateTime(DateUtil.date(createTime));
        tokenEntity.setTokenExpiration(DateUtil.date(expireTime));

        int insert = tokenDao.insert(tokenEntity);
        if (insert > 0) {
            log.info("token信息存入数据库成功");
        } else {
            log.info("token信息存入数据库失败");
        }
    }


    public Map<String, Object> isToken(MyUserEntity userEntity) {
        Map<String, Object> map = new HashMap<>();

        //查出所有token
        List<TokenEntity> tokenEntities = tokenDao.selectList(null);
        for (TokenEntity tokenEntity : tokenEntities) {
            //挨个查看token是否存在
            //把所有的token穿进去,只验证这个token是否存在,不管是谁的
            R r = verifyLoginStatus(tokenEntity.getTokenValue());
            //找到这个token了
            if (r.getCode() == 200) {
                map.put("token", r.getData());
                map.put("status", true);
                return map;
            }
        }
        map.put("token", null);
        map.put("status", false);
        return map;
    }


    /**
     * 验证token
     *
     * @param token token
     * @return R
     */
    public R verifyLoginStatus(String token) {
        //验证token是否存在
        QueryWrapper<TokenEntity> qw = new QueryWrapper<>();
        qw.eq("token_value", token);
        TokenEntity tokenEntity = tokenDao.selectOne(qw);
        if (ObjectUtil.isNull(tokenEntity)) {
            log.info("token不存在");
            return R.error("token不存在");
        }

        //验证token是否过期
        if (tokenEntity.getTokenExpiration().before(new Date())) {
            log.info("token已过期");
            return R.error("token已过期");
        }

        return R.ok(token);
    }
}

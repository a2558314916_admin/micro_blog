package com.mucd.userservice.openfeign;

import com.mucd.blog.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author mucd
 */
@Component
@FeignClient(value = "blog-thread")
public interface UserUpload {
    /**
     * 上传头像
     * @param file
     * @return
     */
    @RequestMapping(value = "/picture/upload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R uploadHead(@RequestPart("file") MultipartFile file);

}

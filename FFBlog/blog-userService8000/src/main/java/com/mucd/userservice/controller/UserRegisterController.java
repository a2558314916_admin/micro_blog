package com.mucd.userservice.controller;

import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.result.R;
import com.mucd.blog.util.ThreadLocalUtils;
import com.mucd.userservice.service.UserRegisterServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author mucd
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserRegisterController {

    @Resource
    private UserRegisterServiceImpl userRegisterService;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @RequestMapping("/register")
    public R register(@RequestBody MyUserEntity user) {
        ThreadLocalUtils.set("user",user);

        return  userRegisterService.register();
    }
}

package com.mucd.userservice.controller;

import com.mucd.blog.result.R;
import com.mucd.userservice.openfeign.UserUpload;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author mucd
 */
@RestController
@RequestMapping("/user")
public class UserUploadController {

    @Resource
    private UserUpload userUpload;

    @RequestMapping("/uploadHead")
    public R uploadHead(MultipartFile file) {
        return userUpload.uploadHead(file);
    }
}

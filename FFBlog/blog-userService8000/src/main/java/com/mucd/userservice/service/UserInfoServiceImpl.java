package com.mucd.userservice.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mucd.blog.annontion.NotNull;
import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.result.R;
import com.mucd.blog.sentinel.ArrayRemoval;
import com.mucd.blog.util.AssertUtil;
import com.mucd.userservice.dao.UserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;

/**
 * @author mucd
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class UserInfoServiceImpl {

    @Resource
    private UserDao userDao;

    /**
     * 根据用户名查询个人信息
     */
    public R getUserInfo(@NotNull String username) {
        QueryWrapper<MyUserEntity> myUserEntityQueryWrapper = new QueryWrapper<>();
        myUserEntityQueryWrapper.eq("user_name", username);

        //判断用户是否重复
        Boolean repeat = isRepeat(myUserEntityQueryWrapper, username);
        AssertUtil.isRepeat(repeat);

        MyUserEntity myUserEntity = userDao.selectOne(myUserEntityQueryWrapper);

        return R.ok(myUserEntity);
    }

    /**
     * 判断用户是否有重复
     * true 有一条数据则用户不重复
     * false 没有数据或者数据大于1用户重复
     *
     * @param queryWrapper 条件
     * @param name         用户名
     * @return
     */
    private Boolean isRepeat(QueryWrapper<MyUserEntity> queryWrapper, String name) {
        QueryWrapper<MyUserEntity> userName = queryWrapper.eq("user_name", name);
        List<MyUserEntity> myUserEntities = userDao.selectList(userName);
        return myUserEntities.size() <= 1;
    }


    /**
     * 修改用户信息
     *
     * @param user 用户数据
     * @return r
     */
    public R update(@NotNull MyUserEntity user) {
        int i = userDao.updateById(user);

        if (i > 0) {
            return R.ok("修改完成");
        }
        return R.error("修改失败");
    }

    /**
     * 添加用户喜欢的标签
     *
     * @param tagsName 标签名
     * @param username
     * @return
     */
    public R addTagsName(String tagsName, String username) throws JsonProcessingException {
        MyUserEntity userName = userDao.selectOne(new QueryWrapper<MyUserEntity>().eq("user_name", username));
        String userLikeTag = userName.getUserLikeTag();
        //去重
        HashSet<String> removal = ArrayRemoval.removal(userLikeTag);

        //已添加够过此标签
        if (removal.toString().contains(tagsName)) {
            return R.ok("waning");
        }
        //将标签添加到集合中
        removal.add(tagsName);
        //将集合转换为json
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(removal);
        userName.setUserLikeTag(json);
        //执行添加
        int i = userDao.updateById(userName);
        if (i <= 0) {
            return R.error("添加失败");
        }

        return R.ok("操作成功");
    }

    /**
     * 获取用户喜欢的标签
     *
     * @return
     */
    public R isHave(String username) {
        MyUserEntity userName = userDao.selectOne(new QueryWrapper<MyUserEntity>().eq("user_name", username));
        return R.ok(userName.getUserLikeTag());
    }

    public R updateHeader(MyUserEntity user) {
        String str = userDao.updateById(user) > 0 ? "修改成功" : "修改失败";

        return R.ok(str);
    }
}

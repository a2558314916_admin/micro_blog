package com.mucd.userservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mucd
 * @date 2022/5/31 2022/5/31
 */
@RestController
public class MyController {

    @Value("${user.name1}")
    String name ;

    @RequestMapping("/name")
    public String name(){
        return name;
    }

}

package com.mucd.userservice;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.mucd")
@EnableDiscoveryClient
@MapperScan("com.mucd.userservice.dao")
@Slf4j
@EnableFeignClients
public class UserServiceApplication {

    public static void main(String[] args) {
        log.info("8000端口启动成功");
        SpringApplication.run(UserServiceApplication.class, args);
    }

}

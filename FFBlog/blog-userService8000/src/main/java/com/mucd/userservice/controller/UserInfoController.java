package com.mucd.userservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mucd.blog.entity.MyUserEntity;
import com.mucd.blog.result.R;
import com.mucd.userservice.service.UserInfoServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author mucd
 */
@RestController
@Slf4j
@RequestMapping("/user")
public class UserInfoController {

    @Resource
    private UserInfoServiceImpl userInfoService;


    @RequestMapping("/updateHead")
    public R updateHeader(@RequestBody MyUserEntity user) {
        return userInfoService.updateHeader(user);
    }

    @RequestMapping("/userInfo/{username}")
    public R userInfo(@PathVariable("username") String username) {
        return userInfoService.getUserInfo(username);
    }

    @RequestMapping("/updateUserInfo")
    public R updateUserInfo(@RequestBody MyUserEntity user) {
        return userInfoService.update(user);
    }

    @RequestMapping("/addLikeTag")
    public R addLikeTag(@RequestParam String tagsName, @RequestParam String username) throws JsonProcessingException {
        return userInfoService.addTagsName(tagsName, username);
    }

    /**
     * 获取用户喜欢的标签
     * @return
     */
    @RequestMapping("/ishave")
    public R isHave(@RequestParam String username) {
        return userInfoService.isHave(username);
    }
}

package com.mucd.blogblogs7000.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mucd.blog.entity.ColumnEntity;

/**
 * @Author mucd
 */
public interface ColumnDao extends BaseMapper<ColumnEntity> {
}

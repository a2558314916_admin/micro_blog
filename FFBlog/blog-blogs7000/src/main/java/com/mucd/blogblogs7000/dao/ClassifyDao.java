package com.mucd.blogblogs7000.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mucd.blog.entity.ClassifyEntity;

/**
 * @author mucd
 */
public interface ClassifyDao extends BaseMapper<ClassifyEntity> {
}

package com.mucd.blogblogs7000;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "com.mucd")
@EnableDiscoveryClient
@MapperScan("com.mucd.blogblogs7000.dao")
@EnableScheduling
@EnableCaching
@Slf4j
public class BlogBlogs7000Application {

    public static void main(String[] args) {
        SpringApplication.run(BlogBlogs7000Application.class, args);
        log.info("7000端口启动成功");
    }

}

package com.mucd.blogblogs7000.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mucd.blog.entity.ArticleEntity;

public interface BlogsDao extends BaseMapper<ArticleEntity> {
}

package com.mucd.blogblogs7000.service;

import com.mucd.blog.sentinel.ContinuousDelayBaseService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class ReqTimesService extends ContinuousDelayBaseService<Integer> {


    @Override
    protected void done(Integer value) {

        log.info("次数:{}", value);

    }
}

package com.mucd.blogblogs7000.controller;

import com.mucd.blog.entity.ClassifyEntity;
import com.mucd.blog.result.R;
import com.mucd.blogblogs7000.service.ClassifyServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author mucd
 */
@RestController
@Slf4j
@RequestMapping("/classify")
public class ClassifyController {

    @Resource
    private ClassifyServiceImpl classifyService;


    @RequestMapping("/getClassifyByUserByBlogs")
    public R getClassifyByUserByBlogs(@RequestParam("username") String username) {
        return classifyService.getClassifyByUserByBlogs(username);
    }

    /**
     * 获取所有分类
     * @return
     */
    @RequestMapping("/list")
    public R list(){
        return classifyService.list();
    }

    /**
     * 新增分类
     * @param classifyEntity
     * @return
     */
    @RequestMapping("/save")
    public R save(@RequestBody ClassifyEntity classifyEntity){
        return classifyService.save(classifyEntity);
    }

    /**
     * 删除分类
     * @param classId
     * @return
     */
    @RequestMapping("/delete")
    public R delete(@RequestParam List<Integer> classId){
        return classifyService.deletes(classId);
    }

    /**
     * 修改分类
     * @param classifyEntity
     * @return
     */
    @RequestMapping("/update")
    public R update(@RequestBody ClassifyEntity classifyEntity){
        return classifyService.update(classifyEntity);
    }

}

package com.mucd.blogblogs7000.controller;

import com.mucd.blog.entity.ArticleEntity;
import com.mucd.blog.result.R;
import com.mucd.blogblogs7000.service.BlogsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文章
 *
 * @param <K>
 * @author mucd
 */
@RestController
@Slf4j
@RequestMapping("/blogs")
public class BlogsController<K> {
    @Autowired
    private BlogsService blogsService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @RequestMapping("/getArticleCountByClassify")
    public R getArticleCountByClassify(@RequestParam("username") String username) {
        return blogsService.getArticleCountByClassify(username);
    }

    @RequestMapping("/getArticleByClassify")
    public R getArticleByClassify(@RequestParam("classify") String classify, @RequestParam("username") String username) {
        return blogsService.getArticleByClassify(classify, username);
    }

    /**
     * 发布文章
     *
     * @param articleEntity
     * @return
     */
    @RequestMapping("/save")
    public R save(@RequestBody ArticleEntity articleEntity) {
        return blogsService.save(articleEntity);
    }

    /**
     * 删除文章
     *
     * @param articleId
     * @return
     */
    @RequestMapping("/delete")
    public R deletes(@RequestParam List<Long> articleId) {
        return blogsService.delete(articleId);
    }

    /**
     * 修改文章
     *
     * @param articleEntity
     * @return
     */
    @RequestMapping("/update")
    public R update(@RequestBody ArticleEntity articleEntity) {
        return blogsService.update(articleEntity);
    }

    /**
     * 分页查询文章
     *
     * @param page     当前页
     * @param pageSize 每页显示的文章条数
     * @return
     */
    @RequestMapping("/select/{page}/{pageSize}")
    public R select(@PathVariable Integer page, @PathVariable Integer pageSize) {
        return blogsService.select(page, pageSize);
    }

    /**
     * 获取用户博客下面的所有标签
     *
     * @return
     */
    @RequestMapping("/getUserOfBlogsOfTags")
    public R getUserOfBlogsOfTags(@RequestParam String username) {
        return blogsService.getUserOfBlogsOfTags(username);

    }

    /**
     * 按照标签名获取文章信息
     *
     * @return
     */
    @RequestMapping("/getBlogsByTagName")
    public R getBlogsByTagName(@RequestParam String username, @RequestParam String tag) {
        return blogsService.getBlogsByTagName(username, tag);
    }
}

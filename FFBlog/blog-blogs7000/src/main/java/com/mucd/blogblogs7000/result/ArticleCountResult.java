package com.mucd.blogblogs7000.result;

import lombok.Data;

/**
 * @author mucd
 */
@Data
public class ArticleCountResult {
    private String name;
    private Integer value;
}

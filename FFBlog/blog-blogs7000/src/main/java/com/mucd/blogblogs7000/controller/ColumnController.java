package com.mucd.blogblogs7000.controller;

import cn.hutool.core.util.StrUtil;
import com.mucd.blog.entity.ColumnEntity;
import com.mucd.blog.result.R;
import com.mucd.blogblogs7000.service.ColumnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 栏目
 *
 * @Author mucd
 */
@RestController
@Slf4j
@RequestMapping("/column")
public class ColumnController {

    @Resource
    private ColumnService columnService;

    /**
     * 查询栏目
     *
     * @return
     */
    @RequestMapping("/list")
    public List<ColumnEntity> list() {
        return columnService.saveColumn();
    }

    @CacheEvict(value = "columnList", allEntries = true)
    @RequestMapping("/qwe")
    public void saveColumn1() {


    }


    /**
     * 新增栏目
     *
     * @return
     */
    @RequestMapping("/save")
    public R save(@RequestBody ColumnEntity columnEntity) {
        return columnService.save(columnEntity);
    }

    /**
     * 删除栏目
     *
     * @param columnId
     * @return
     */
    @RequestMapping("/delete")
    public R save(@RequestParam List<Integer> columnId) {
        return columnService.delete(columnId);
    }

    /**
     * 修改栏目
     *
     * @param columnEntity
     * @return
     */
    @RequestMapping("/update")
    public R update(@RequestBody ColumnEntity columnEntity) {
        return columnService.updateById(columnEntity);
    }

}

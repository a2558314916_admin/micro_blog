package com.mucd.blogblogs7000.service;

import com.mucd.blog.entity.TagsEntity;
import com.mucd.blog.result.R;
import com.mucd.blogblogs7000.dao.TagsDao;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
@Log4j2
public class TagsService {

    @Resource
    private TagsDao tagsDao;

    /**
     * 查询文章标签
     * @return
     */
    @Cacheable(value = "bolgTags")
    public R list() {
        List<TagsEntity> tagsEntities = tagsDao.selectList(null);
        return R.ok(tagsEntities);
    }

    /**
     * 新增文章标签
     * @param tagsEntity
     * @return
     */
    @CacheEvict(value = "bolgTags", allEntries = true)//清除文章标签缓存
    public R save(TagsEntity tagsEntity) {
        int insert = tagsDao.insert(tagsEntity);
        if (insert<=0){
            return R.error("新增文章标签失败");
        }
        return R.ok("操作成功");
    }

    /**
     * 删除文章标签
     * @param tagsId
     * @return
     */
    @CacheEvict(value = "bolgTags", allEntries = true)//清除文章标签缓存
    public R delete(List<Integer> tagsId) {
        int i = tagsDao.deleteBatchIds(tagsId);
        if (i<=0){
            return R.error("删除文章标签失败");
        }
        return R.ok("操作成功");
    }

    /**
     * 修改文章标签
     * @param tagsEntity
     * @return
     */
    @CacheEvict(value = "bolgTags", allEntries = true)//清除文章标签缓存
    public R update(TagsEntity tagsEntity) {
        int i = tagsDao.updateById(tagsEntity);
        if (i<=0){
            return R.error("修改文章标签失败");
        }
        return R.ok("操作成功");
    }
}

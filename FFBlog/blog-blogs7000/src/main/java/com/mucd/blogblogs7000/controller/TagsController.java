package com.mucd.blogblogs7000.controller;

import com.mucd.blog.entity.TagsEntity;
import com.mucd.blog.result.R;
import com.mucd.blogblogs7000.service.TagsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 文章标签
 *
 *  @author yiming
 */
@RestController
@Slf4j
@RequestMapping("/tags")
public class TagsController {
    @Autowired
    private TagsService tagsService;

    /**
     * 查询文章标签
     *
     * @return
     */
    @RequestMapping("/list")
    public R list() {
        return tagsService.list();
    }

    /**
     * 新增文章标签
     *
     * @param tagElement
     * @return
     */
    @RequestMapping("/save")
    public R save(@RequestBody TagsEntity tagElement) {
        return tagsService.save(tagElement);
    }

    /**
     * 删除文章标签
     *
     * @return
     */
    @RequestMapping("/delete")
    public R delete(@RequestParam List<Integer> tagsId) {
        return tagsService.delete(tagsId);
    }

    /**
     * 修改文章标签
     * @param tagsEntity
     * @return
     */
    @RequestMapping("/update")
    public R update(@RequestBody TagsEntity tagsEntity) {
        return tagsService.update(tagsEntity);
    }


    /**
     * 清除文章标签缓存
     */
    @RequestMapping("/cache")
    @CacheEvict(value = "bolgTags", allEntries = true)
    public void cache() {
    }

}

package com.mucd.blogblogs7000.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mucd.blog.entity.TagsEntity;

public interface TagsDao extends BaseMapper<TagsEntity> {
}

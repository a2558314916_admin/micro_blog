package com.thread.blogsthreadparties4000.controller;

import com.mucd.blog.result.R;
import com.thread.blogsthreadparties4000.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author mucd
 */
@RestController
@RequestMapping("/picture")
public class PictureController {

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "/upload")
    public R upload(@RequestParam("file") MultipartFile file) {
        R upload = pictureService.uploadHead(file);
//        Result<Sing>tr upload = pictureService.upload(file);
        System.out.println("upload = " + upload);
        return upload;
    }

}
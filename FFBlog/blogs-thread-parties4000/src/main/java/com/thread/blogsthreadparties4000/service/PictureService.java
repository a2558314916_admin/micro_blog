package com.thread.blogsthreadparties4000.service;

import com.mucd.blog.result.R;
import com.mucd.blog.result.Result;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {
    /**
     * 用户头像上传
     * @param file
     * @return
     */
    Result<String> upload(MultipartFile file);
    R uploadHead(MultipartFile file);
}

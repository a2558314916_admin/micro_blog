package com.mucd.blogbackmanage.controller;

import com.mucd.blog.entity.ArticleEntity;
import com.mucd.blog.result.R;
import com.mucd.blogbackmanage.feign.FeignArticleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author mucd
 */
@RequestMapping("/blogsManage")
@RestController
@Slf4j
public class BlogsManageController {
    @Resource
    private FeignArticleService feignArticleService;


    @RequestMapping("/save")
    public R save(@RequestBody ArticleEntity articleEntity){
        return feignArticleService.save(articleEntity);
    }
}

package com.mucd.blogbackmanage.feign;

import com.mucd.blog.entity.ArticleEntity;
import com.mucd.blog.result.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(value = "blog-blogs")
public interface FeignArticleService {
    @RequestMapping("/blogs/save")
    public R save(ArticleEntity articleEntity);
}
